package com.devcamo.task62j15restapi.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamo.task62j15restapi.models.District;
import com.devcamo.task62j15restapi.models.Province;
import com.devcamo.task62j15restapi.models.Ward;
import com.devcamo.task62j15restapi.responsitory.IDistrictResponsitory;
import com.devcamo.task62j15restapi.responsitory.IProvinceResponsitory;
import com.devcamo.task62j15restapi.responsitory.IWardResponsitory;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
    @Autowired
    IDistrictResponsitory iDistrictResponsitory;
    @Autowired
    IProvinceResponsitory iProvinceResponsitory;
    @Autowired
    IWardResponsitory iWardResponsitory;

    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvince() {
        try {
            List<Province> list = new ArrayList<Province>();
            iProvinceResponsitory.findAll().forEach(list::add);
            return new ResponseEntity<List<Province>>(list, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts")
    public ResponseEntity<Set<District>> getDistrictById(@RequestParam(name = "province_id") int province_id) {
        try {
            // Assuming findByProvinceId returns a Province with associated districts
            Province province = iProvinceResponsitory.findById(province_id);

            if (province != null) {
                // Assuming that 'districts' is a Set<District> field in the Province class
                Set<District> districts = province.getDistricts();
                return new ResponseEntity<>(districts, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards")
    public ResponseEntity<Set<Ward>> getWardByDistrictId(@RequestParam(name = "district_id") int district_id) {
        try {

            District district = iDistrictResponsitory.findById(district_id);

            if (district != null) {
                return new ResponseEntity<>(district.getWards(), HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
