package com.devcamo.task62j15restapi.responsitory;

import com.devcamo.task62j15restapi.models.Province;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProvinceResponsitory extends JpaRepository<Province, Integer> {
    Province findById(int id); // Corrected method name
}
