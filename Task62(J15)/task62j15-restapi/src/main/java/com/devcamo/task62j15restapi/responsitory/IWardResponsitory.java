package com.devcamo.task62j15restapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamo.task62j15restapi.models.Ward;

public interface IWardResponsitory extends JpaRepository<Ward, Integer> {
    //Ward findById(int id);
}
