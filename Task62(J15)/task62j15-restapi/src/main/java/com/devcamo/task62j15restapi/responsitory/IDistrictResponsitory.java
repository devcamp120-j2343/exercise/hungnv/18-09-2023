package com.devcamo.task62j15restapi.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamo.task62j15restapi.models.District;

public interface IDistrictResponsitory extends JpaRepository<District,Integer> {
    District findById(int id);
}
