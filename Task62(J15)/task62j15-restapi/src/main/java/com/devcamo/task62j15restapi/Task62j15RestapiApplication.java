package com.devcamo.task62j15restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task62j15RestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task62j15RestapiApplication.class, args);
	}

}
